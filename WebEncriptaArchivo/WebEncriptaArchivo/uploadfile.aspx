﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="uploadfile.aspx.cs" Inherits="uploadfile" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style3 {
            height: 23px;
            width: 340px;
        }
        .auto-style4 {
            height: 23px;
            width: 118px;
        }
        .auto-style5 {
            width: 340px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="text-align: center; background-color: #172A3D;  width:600px; margin:0 auto">

             <asp:Image ID="Image1" runat="server" ImageUrl="~/img/fflogo.png" />

        
    <div style="border-color: #172A3D; border-style:solid; border-width:medium; text-align: center; background-color:white;">
   
        <p style="font-family: Calibri">Datos de Contacto</p>
        <table align="center" style="width: 438px; font-family: Calibri; height: 128px;"><tr><td class="auto-style4">Raz&oacute;n Social: </td><td class="auto-style5" style="text-align: left">
            <asp:TextBox ID="txtRS" runat="server" Width="205px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RFVRS" runat="server" ControlToValidate="txtRS" ErrorMessage="*Completar" Font-Size="9pt" ForeColor="#CC0000" ValidationGroup="Validaciones"></asp:RequiredFieldValidator>
            </td></tr>
            <tr><td class="auto-style4">Nombre: </td><td class="auto-style5" style="text-align: left">
                <asp:TextBox ID="txtNombre" runat="server" Width="205px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RFVNombre" runat="server" ControlToValidate="txtNombre" ErrorMessage="*Completar" Font-Size="9pt" ForeColor="#CC0000" ValidationGroup="Validaciones"></asp:RequiredFieldValidator>
                <br />
                </td></tr>
             <tr><td class="auto-style4">Tel&eacute;fono: </td><td class="auto-style5" style="text-align: left">
                <asp:TextBox ID="txtTelefono" runat="server" Width="205px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RFVTelefono" runat="server" ControlToValidate="txtTelefono" ErrorMessage="*Completar" Font-Size="9pt" ForeColor="#CC0000" ValidationGroup="Validaciones"></asp:RequiredFieldValidator>
                </td></tr>
            <tr><td class="auto-style4">Email: </td><td class="auto-style5" style="text-align: left">
                <asp:TextBox ID="txtEmail" runat="server" Width="205px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RFVEmail" runat="server" ControlToValidate="txtEmail" ErrorMessage="*Completar" Font-Size="9pt" ForeColor="#CC0000" ValidationGroup="Validaciones"></asp:RequiredFieldValidator>
                <br />
                </td></tr>
           
             <tr><td class="auto-style4"></td><td class="auto-style3">
                <asp:RegularExpressionValidator ID="regexEmail" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtEmail" ErrorMessage="Formato invalido" Font-Size="9pt" ForeColor="#CC0000" ValidationGroup="Validaciones"></asp:RegularExpressionValidator>
                </td></tr>
            <tr><td class="auto-style4">Tipo Archivo: </td><td style="text-align: left" class="auto-style5">
                <asp:DropDownList ID="ddlTipo" runat="server">
                    <asp:ListItem>Documentos Varios</asp:ListItem>
                    <asp:ListItem>Perfiles</asp:ListItem>
                    <asp:ListItem>Tarjetas</asp:ListItem>
                </asp:DropDownList>
                </td></tr>
        </table>
   

<asp:FileUpload ID="FileUpload1" runat="server" Width="374px" />
    
        <asp:Image ID="Image4" runat="server" Height="34px" ImageUrl="~/img/cargando.gif" Visible="False" Width="34px" />
    
        <br />
        <asp:Label ID="Label1" runat="server" ForeColor="#003399" Font-Size="Small">Extensiones soportadas: &quot;.txt&quot;, &quot;.doc&quot;, &quot;.docx&quot;, &quot;.jpg&quot;,&quot;.pdf&quot;, &quot;.csv&quot;, &quot;.bmp&quot;, &quot;.gif&quot;, &quot;.png&quot;, &quot;.xls&quot;, &quot;.xlsx&quot;</asp:Label>
        <br />
        <br />
        <asp:Button ID="Ingresar" runat="server" OnClick="Ingresar_Click" Text="Aceptar" ValidationGroup="Validaciones" />
    
        <br />
        <br />
        <asp:Image ID="Image2" runat="server" ImageUrl="~/img/pci_dss.png" />
        <asp:Image ID="Image3" runat="server" ImageUrl="~/img/TLS.png" />
    
    </div>
            
                    <p><asp:HyperLink ID="hlterminos" runat="server" Target="_blank" Font-Names="Arial" Font-Size="8pt" ForeColor="DimGray" NavigateUrl="https://www.furlong-fox.com.ar/terminos-y-condiciones/">Términos y Condiciones</asp:HyperLink></p>

            </div>


    </form>
</body>
</html>
