﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class uploadfile : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Image4.Visible = false;
    }
    protected void Ingresar_Click(object sender, EventArgs e)
    {
        
        if (IsPostBack && Page.IsValid)
        {
            Boolean fileOK = false;
            String path = Server.MapPath("~/Archivos/");
            if (FileUpload1.HasFile)
            {
                String fileExtension =
                    Path.GetExtension(FileUpload1.FileName).ToLower();
                String[] allowedExtensions = { ".txt", ".doc", ".docx", ".jpg", ".pdf", ".csv", ".bmp", ".gif", ".png", ".xls", ".xlsx" };
                for (int i = 0; i < allowedExtensions.Length; i++)
                {
                    if (fileExtension == allowedExtensions[i])
                    {
                        fileOK = true;
                    }
                }
            }

            if (fileOK)
            {
                try
                {

                    Image4.Visible = true;
                    string nonencryptedState = path + Path.GetFileName(FileUpload1.FileName);

					Criptografia.Criptografia_Encrypt(nonencryptedState, encryptedState);

                   
                    string encryptedState = path + Path.GetFileName(FileUpload1.PostedFile.FileName) + "_enc" + Path.GetExtension(this.FileUpload1.PostedFile.FileName);

					FileUpload1.PostedFile.SaveAs(encryptedState);
                
                    Image4.Visible = false;
                    string script = "alert(\"Archivo encriptado y cargado con éxito!\");";
                    ScriptManager.RegisterStartupScript(this, GetType(),
                                          "ServerControlScript", script, true);
                 
                   
                    clsMail mail = new clsMail();
                    mail.EnvioMail("corporate@furlong-fox.com.ar", "implementacionescco@furlongfox.zohosupport.com", "Alta " + ddlTipo.Text, "Datos de Contacto: <br/><br/>Razon Social: " + txtRS.Text + "<br/>Nombre: " + txtNombre.Text + "<br/>Email: " + txtEmail.Text + "<br/>Telefono: " + txtTelefono.Text + "<br/>Tipo de Archivo: " + ddlTipo.Text + "<br/><br/>Se ha cargado el siguiente archivo: " + "<a href=\"" + "https://www.furlong-fox.com.ar/archivos/archivos/" + Path.GetFileName(FileUpload1.PostedFile.FileName) + "_enc" + Path.GetExtension(this.FileUpload1.PostedFile.FileName) + "\"" + " download>Click aqui para descargar</a>", "smtp.gmail.com", "corporate@furlong-fox.com.ar", AccesoDatos.GetDatos.Mail.Pass);
                   
                    mail.EnvioMail("corporate@furlong-fox.com.ar", txtEmail.Text, "Furlong-Fox Carga de archivo", txtNombre.Text + ",<br/><br/>Hemos recibido su archivo. El mismo ha sido encriptado con éxito. <br/> Estaremos procesando los datos a la brevedad.<br/><br/>Muchas gracias.", "smtp.gmail.com", "corporate@furlong-fox.com.ar", AccesoDatos.GetDatos.Mail.Pass);

                }
                catch (Exception ex)
                {

                    string script = "alert(\"No se puede cargar el archivo.\");";
                    ScriptManager.RegisterStartupScript(this, GetType(),
                                          "ServerControlScript", script, true);


                }
            }
            else
            {

                string script = "alert(\"No se aceptan archivos de este tipo.\");";
                ScriptManager.RegisterStartupScript(this, GetType(),
                                      "ServerControlScript", script, true);
            }
        }


    }
}