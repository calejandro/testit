using System;

public class clsMail
{
    public void EnvioMail(string remitente, string destinatario, string asunto, string cuerpo, string smtphost, string login, string pass, string adjuntos = null)
    {
        System.Net.Mail.MailMessage correo = new System.Net.Mail.MailMessage();
        correo.From = new System.Net.Mail.MailAddress(remitente);
        correo.To.Add(destinatario);
        correo.Subject = asunto;
        correo.Body = cuerpo;
        correo.IsBodyHtml = true;
        correo.Priority = System.Net.Mail.MailPriority.Normal;
        System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
        smtp.Host = smtphost;
        smtp.Port = 587;
        smtp.Credentials = new System.Net.NetworkCredential(login, pass);
        smtp.EnableSsl = true;
        smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
        //if (adjuntos != null){
          //  foreach (string element in adjuntos){
            //    if (!string.IsNullOrEmpty(element)){
              //      correo.Attachments.Add(new System.Net.Mail.Attachment(element));
                //    }
                //}
        //}
        try
        {
            smtp.Send(correo);
            //MsgBox("Mensaje enviado satisfactoriamente");
        }
        catch (Exception ex)
        {
            //MsgBox("ERROR: " + ex.Message);
        }
    }

    public string Test()
    {
        return "OK";
    }
}
