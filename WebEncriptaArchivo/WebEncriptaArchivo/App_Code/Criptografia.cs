﻿using System;
using System.IO;
using System.Security;
using System.Security.Cryptography;
using System.Runtime;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for Criptografia
/// </summary>
public class Criptografia
{

    const string exceptionMessage = "Error. Falló el proceso de criptografía.";

    public static void Criptografia_Encrypt(string inputFile, string outputFile)
    {

        try
        {


            string password = AccedoDatos.GetDatos.Password;
            string salt = AccedoDatos.GetDatos.Salt;
            string encryptedFile = outputFile;

            RijndaelManaged cspRijndael = new RijndaelManaged();
            cspRijndael.KeySize = 256;
            cspRijndael.BlockSize = 256;
            cspRijndael.Mode = CipherMode.CBC;

            Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(password, Encoding.ASCII.GetBytes(salt), 999);
            cspRijndael.IV = key.GetBytes(cspRijndael.BlockSize / 8);
            cspRijndael.Key = key.GetBytes(cspRijndael.KeySize / 8);


            FileStream Criptografia_fileEncrypt = new FileStream(encryptedFile, FileMode.Create);

            
            CryptoStream cs = new CryptoStream(Criptografia_fileEncrypt, cspRijndael.CreateEncryptor(cspRijndael.Key, cspRijndael.IV), CryptoStreamMode.Write);

            //write to new file 
            FileStream Criptografia_In = new FileStream(inputFile, FileMode.Open);

            int data;
            while ((data = Criptografia_In.ReadByte()) != -1)
                cs.WriteByte((byte)data);

            //close 
            Criptografia_In.Close();
            cs.Close();
            Criptografia_fileEncrypt.Close();


        }
        catch (Exception ex)
        {

            //error message
            exceptionMessage.ToString();

        }

    }

    

}